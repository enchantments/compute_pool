tls {
  verify_https_client    = true
  verify_server_hostname = true

  http      = true
  rpc       = true

  ca_file   = "/etc/tls/ca.pem"
  key_file  = "/etc/tls/key.pem"
  cert_file = "/etc/tls/cert.pem"
}
