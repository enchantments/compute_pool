data "google_compute_image" "compute_node" {
  family  = "compute-node"
  project = var.project
}

resource "google_compute_instance" "compute" {
  count = var.instances

  name         = "compute-${count.index + 1}"
  machine_type = "n1-standard-1"
  project      = var.project
  zone         = var.zone

  tags = ["consul"]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.compute_node.self_link
    }
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection {
    type        = "ssh"
    host        = self.network_interface.0.access_config.0.nat_ip
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/tls",
      "sudo mkdir -p /etc/tls /etc/consul /etc/nomad",
    ]
  }

  provisioner "file" {
    destination = "/tmp/tls/key.pem"
    content     = tls_private_key.instance[count.index].private_key_pem
  }

  provisioner "file" {
    destination = "/tmp/tls/cert.pem"
    content = join("\n", flatten([
      vault_pki_secret_backend_sign.compute[count.index].certificate,
      vault_pki_secret_backend_sign.compute[count.index].ca_chain,
    ]))
  }

  provisioner "file" {
    destination = "/tmp/tls/ca.pem"
    content     = vault_pki_secret_backend_sign.compute[count.index].ca_chain[length(vault_pki_secret_backend_sign.compute[count.index].ca_chain) - 1]
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "${path.module}/files"
  }

  provisioner "file" {
    destination = "/tmp/files/consul/datacenter.json"
    content = templatefile("${path.module}/templates/consul/datacenter.json", {
      datacenter = var.datacenter
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/gossip.json"
    content = templatefile("${path.module}/templates/consul/gossip.json", {
      gossip_key = var.control_plane_consul_gossip_key
    })
  }

  provisioner "file" {
    destination = "/tmp/files/consul/retry-join.json"
    content = templatefile("${path.module}/templates/consul/retry-join.json", {
      project = var.project
    })
  }

  provisioner "file" {
    destination = "/tmp/files/nomad/datacenter.hcl"
    content = templatefile("${path.module}/templates/nomad/datacenter.hcl", {
      datacenter = var.datacenter
    })
  }

  provisioner "file" {
    destination = "/tmp/files/nomad/region.hcl"
    content = templatefile("${path.module}/templates/nomad/region.hcl", {
      region = var.region
    })
  }


  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/tls/* /etc/tls",
      "sudo mv /tmp/files/consul/* /etc/consul",
      "sudo mv /tmp/files/nomad/* /etc/nomad",
      "sudo mv /tmp/files/systemd/* /lib/systemd/system",
      "sudo mv /tmp/files/networkd/* /lib/systemd/network",
      "sudo mv /tmp/files/environment /etc/environment",
      "sudo systemctl daemon-reload",
      "sudo systemctl restart systemd-networkd",
      "sudo systemctl enable consul.path",
      "sudo systemctl start consul.path",
      "sudo systemctl enable nomad.path",
      "sudo systemctl start nomad.path",
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    scripts = [
      "${path.module}/scripts/nomad-drain.sh",
      "${path.module}/scripts/consul-leave.sh",
    ]
  }
}
