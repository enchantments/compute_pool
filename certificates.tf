resource "tls_private_key" "instance" {
  count       = var.instances
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "instance" {
  count           = var.instances
  key_algorithm   = "ECDSA"
  private_key_pem = element(tls_private_key.instance.*.private_key_pem, count.index)

  subject {
    common_name = "Compute Pool Instance"
  }

  dns_names = [
    "server.${var.datacenter}.consul",
    "client.${var.region}.nomad",
    "localhost",
  ]

  ip_addresses = [
    "127.0.0.1"
  ]
}

resource "vault_pki_secret_backend_sign" "compute" {
  count = var.instances

  backend = var.pki_backend_path
  name    = var.pki_role_name

  csr         = tls_cert_request.instance[count.index].cert_request_pem
  common_name = tls_cert_request.instance[count.index].subject[0].common_name
}
