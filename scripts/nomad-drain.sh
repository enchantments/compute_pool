#!/bin/bash

set -e

[ "$(sudo systemctl is-active nomad)" == "active" ] || exit 0

nomad node drain -enable -self

echo "Disabling nomad.path"
sudo systemctl disable nomad.path
echo "Disabling nomad.service"
sudo systemctl disable nomad.service
echo "Stopping nomad.path"
sudo systemctl stop nomad.path
echo "Stopping nomad.service"
sudo systemctl stop nomad.service
