variable "instances" {}

variable "datacenter" {}
variable "region" {}
variable "control_plane_consul_gossip_key" {}
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "project" {}
variable "zone" {}
variable "pki_vault_address" {}
variable "pki_vault_token" {}
variable "pki_role_name" {}
variable "pki_backend_path" {}

variable "consul_version" {
  default = ""
}

variable "nomad_version" {
  default = ""
}
